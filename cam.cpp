#include "opencv2/opencv.hpp"
#include <iostream>

using namespace cv;
using namespace std;


int main(int, char**)
{
    int numBoards = 1;
    int numCornersHor = 7;
    int numCornersVer = 7;

    int boardMatrix [numCornersHor-1][numCornersVer-1];

    for (int i = 0; i < numCornersHor-1; i++) {
      for (int j = 0; j < numCornersVer-1; j++) {
        boardMatrix[i][j] = 0;
      }
    }

    /**
     * Defines the board direction (Check report for more information)
     * false means vertical (findChessboardCorners points found are in the right order)
     * true means horizontal (findChessboardCorners points are in the orther of the transpost matrix of the vertical points)
     **/
    bool boardOritentation;

    int numSquares = numCornersHor * numCornersVer;
    Size board_sz = Size(numCornersHor, numCornersVer);

    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    Mat edges;
    namedWindow("edges",1);
    namedWindow("testing",1);
    namedWindow("circles",1);


    vector<Point2d> corners;
    Mat frame, img;

    for(;;)
    {

        cap >> frame; // get a new frame from camera
        cap >> img;
        cvtColor(frame, edges, CV_BGR2GRAY);
        GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);

        //Detetar o campo de xadres
        bool found = findChessboardCorners(frame, board_sz, corners, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
        if(found)
        {
          if(corners[numCornersHor*(numCornersVer-1)].y > corners[numCornersVer-1].y){
            std::cout << "Vertical" << std::endl; //Atribuicao Direta
            boardOritentation = true;
          }else{
            std::cout << "Horizontal" << std::endl;
            boardOritentation = false;
          }

          //Calculate the extra points
          //Show the points
          for(vector<Point2d>::size_type i = corners.size() - 1;
            i != (vector<Point2d>::size_type) -1; i--) {


              //cout << i << ": " <<corners[i] << "\n";
              if(i==0 || i==6 || i ==42 || i==48)
                circle(frame, corners[i], 5, 100, 1, 8, 0);
          }

          //Calculate the point pos if inside the board


          //cornerSubPix(frame, corners, Size(11, 11), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
          //drawChessboardCorners(frame, board_sz, corners, found);
      }

      //Detetar os circulos
      vector<Vec3f> circles;
      HoughCircles(edges, circles, HOUGH_GRADIENT, 1, 10,
               100, 30, 1, 60 // change the last two parameters
                              // (min_radius & max_radius) to detect larger circles
               );
      for( size_t i = 0; i < circles.size(); i++ )
      {
          //cout << "Encontrei o circulo " << i << "\n";
          Vec3i c = circles[i];
          circle( img, Point(c[0], c[1]), c[2], Scalar(0,0,255), 3, LINE_AA);
          circle( img, Point(c[0], c[1]), 2, Scalar(0,255,0), 3, LINE_AA); //green
      }

      Canny(edges, edges, 0, 30, 3);
      imshow("edges", edges);
      imshow("circles", img);
      imshow("testing",frame);
      if(waitKey(30) >= 0) break;

    }
    // the camera will be deinitialized automatically in VideoCapture destructor

    std::cout << "Camera Locked!!" << std::endl;


    Point2d topL = corners[0];
    Point2d topR = corners[6];
    Point2d botL = corners[7*6];
    Point2d botR = corners[7*6+6];

    std::cout << "topL" << topL << std::endl;
    std::cout << "topR" << topR << std::endl;
    std::cout << "bopL" << botL << std::endl;
    std::cout << "bopR" << botR << std::endl;


    for(;;){

      for (int i = 0; i < numCornersHor-1; i++) {
        for (int j = 0; j < numCornersVer-1; j++) {
          boardMatrix[i][j] = 0;
        }
      }

      cap >> frame; // get a new frame from camera
      cap >> img;

      cvtColor(frame, edges, CV_BGR2GRAY);

/*
      for(vector<Point2d>::size_type i = corners.size() - 1;
        i != (vector<Point2d>::size_type) -1; i--) {


          //cout << i << ": " <<corners[i] << "\n";
          if(i==0 || i==6 || i ==42 || i==48)
            circle(img, corners[i], 5, 100, 1, 8, 0);
      }*/

      for(vector<Point2d>::size_type i = 0;
        i < 7; i++) {
          line(img,corners[i],corners[7*6+i],(255,0,0),2);
          //cout << i << ": " <<corners[i] << "\n";
      }

      for(vector<Point2d>::size_type i = 0;
        i <= 7*7; i=i+7) {
          line(img,corners[i],corners[i+6],(255,0,0),2);
      }

      /*line(img,corners[0],corners[7*6],(255,0,0),2);
      line(img,corners[0],corners[6],(255,0,0),2);
      line(img,corners[6],corners[7*6+6],(255,0,0),2);
      line(img,corners[7*6],corners[7*6+6],(255,0,0),2);
*/
      //Detetar os circulos
      vector<Vec3f> circles;
      HoughCircles(edges, circles, HOUGH_GRADIENT, 1, 10,
               100, 30, 10, 80 // change the last two parameters
                              // (min_radius & max_radius) to detect larger circles
               );
      vector<Point2i> spots;

      for( size_t i = 0; i < circles.size(); i++ )
      {
          //cout << "Encontrei o circulo " << i << " : "<< circles[i] <<"\n";
          Vec3i c = circles[i];
          circle( img, Point(c[0], c[1]), c[2], Scalar(0,0,255), 3, LINE_AA);
          circle( img, Point(c[0], c[1]), 2, Scalar(0,255,0), 3, LINE_AA); //green

          //Check if inside the area
          if(c[0] > botL.x && c[0] < topR.x && c[1]>topL.y && c[1]<botR.y){
            //std::cout << "ITS INSIDE!!!!" << std::endl;
            int mY = 0;
            int mX = 0;
            //Calculate the square
            //Y's
            for(vector<Point2d>::size_type i = 0; i < 7*7; i=i+7){
              if(c[1] < corners[i].y){
                mY = int(i/7)-1;
                break;
              }
            }
            //X's
            for(vector<Point2d>::size_type i = 0; i < 7; i++){
              if(c[0] < corners[i].x){
                mX = i-1;
                break;
              }
            }

            spots.push_back(Point2i(mX,mY));
          }



      }

      for(int i = 0; i < int(spots.size()); i++){
        std::cout << spots[i] << std::endl;
        boardMatrix[spots[i].y][spots[i].x] = 1;
      }

      for (int i = 0; i < numCornersHor-1; i++) {
        std::cout << "|-|-|-|-|-|-|\n";
        for (int j = 0; j < numCornersVer-1; j++) {
          std::cout << '|'; //<< boardMatrix[i][j]
          if(boardMatrix[i][j] == 0){
            cout << ' ';
          }
          else if(boardMatrix[i][j] == 1){
            cout << 'x';
          }

        }
        std::cout << "|\n";
      }
      //std::cout << "\n\n\n\n\n\n\n\n\n\n\n" << std::endl;

      imshow("testing",frame);
      imshow("circles", img);

      if(waitKey(30) >= 0) break;
    }

    return 0;
}


/**
 * Calculates the next point
 **/
Point2d calculatePoint(Point2d near, Point2d far){


  return Point2d(0,0);
}

/**
 * Calculates the corner points of the board
 * Point 'a' represents the "higher" point and 'b' the "lower"
 * dir represents the direction of the corner to be calculated 'u' as up and 'd' as down
 **/
Point2d getCorners(Point2d a, Point2d b, char dir){

  //Calculate up point in Y with the X coord of the b
  if(dir == 'u'){ //up
      return Point2d(b.x,a.y);
  }

  return Point2d(a.x,b.y);

}

//Print the board

/*
//Print the board
for (int i = 0; i < numCornersHor-1; i++) {
  std::cout << "|-|-|-|-|-|-|\n";
  for (int j = 0; j < numCornersVer-1; j++) {
    std::cout << '|'; //<< boardMatrix[i][j]
    if(boardMatrix[i][j] == 0){
      cout << ' ';
    }
    else if(boardMatrix[i][j] == 1){
      cout << 'x';
    }

  }
  std::cout << "|\n";
}
*/

//Point2d getPos()
